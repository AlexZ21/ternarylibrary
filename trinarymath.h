#ifndef TRINARYMATH_H
#define TRINARYMATH_H

#include "trit.h"

struct TritSum{
    int res;
    int trans;
};

class TrinaryMath
{
    static const char *sumTable[3][3];
    static const char incrTable[3][3];
public:
    TrinaryMath();
    static const char *getSum(int first, int second);
    static const char getIncrease(int first, int second);
};

#endif // TRINARYMATH_H
