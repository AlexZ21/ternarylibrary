#include <QCoreApplication>
#include <QDebug>

#include "trinarylib.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Tryte tr;
    TrinaryInt trint;

    tr.fromString("TTOOIO");
    trint.fromString("IIOIO");
    trint = 6;
    qDebug() << tr.sizeOf() << tr.toString().data();
    qDebug() << trint.sizeOf() << trint.toString().data() << trint.toDecimal();

    TrinaryInt tr2;
    tr2.fromString("IO");
    tr2 = 2;
    qDebug() << tr2.sizeOf() << tr2.toString().data() << tr2.toDecimal();

    qDebug() << "tt " << (trint > tr2);

    if((trint < TrinaryInt()) == Trit::I)
    {
        qDebug() << "test";
    }

    TrinaryInt tr3;
    tr3 = trint / tr2;

    qDebug() << tr3.toString().data() << tr3.toDecimal();


    return a.exec();
}
