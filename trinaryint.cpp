#include "trinaryint.h"
#include <QDebug>

TrinaryInt::TrinaryInt()
{
    value = new Trit[size];
    for(int i = 0; i < size; i++)
        value[i].state = Trit::O;
}

TrinaryInt::TrinaryInt(const TrinaryInt &tr)
{
    value = new Trit[size];
    for(int i = 0; i < size; i++)
        value[i].state = tr.value[i].state;
}

TrinaryInt::~TrinaryInt()
{
    delete value;
}

const TrinaryInt &TrinaryInt::operator =(const TrinaryInt &tr)
{
    for(int i = 0; i < size; i++)
        value[i].state = tr.value[i].state;

    return *this;
}

const TrinaryInt &TrinaryInt::operator =(const int &num)
{
    fromDecimal(num);
    return *this;
}

TrinaryInt TrinaryInt::operator +(const TrinaryInt &tr)
{
    //http://trinary.ru/kb/3d3703c1-75fb-4bf8-9041-a27d34a30508/Troichnaya-arifmetika
    /*
     *
        +1	00	01	1i
        0	0i	00	01
        −1	i1	0i	00
        +	−1	0	+1

     **/

    /*
     * Для примера
     * IOTTTO  -  204
     * 0IOOTO  -   79
     *-------
     * IITTII  - result
     * 000T00  - trans
     *-------
     * IITIII
     * 00T000
     *-------
     * IIIIII
     * OTOOOO
     *-------
     * I0IIII  -  283
     **/

    TrinaryInt result;
    TrinaryInt tmp;

    for(int i = size-1; i >= 0; i--)
    {
        const char *sum = TrinaryMath::getSum(value[i].state, tr.value[i].state);
        result.value[i].state = Trit::getStateFromChar(sum[1]);
        if(i > 0) tmp.value[i-1].state = Trit::getStateFromChar(sum[0]);
    }

    if(tmp.isNull() == Trit::T)
        result = result + tmp;

    return result;
}

TrinaryInt TrinaryInt::operator -(const TrinaryInt &tr)
{
    TrinaryInt res = tr;
    return *this + res.inverse();
}

TrinaryInt TrinaryInt::operator *(const TrinaryInt &tr)
{
    TrinaryInt result;

    for(int n = 0; n < size; n++)
    {
        TrinaryInt tmp;
        for(int i = size-1; i >= 0; i--)
        {
            if(i-n >= 0) tmp.value[i-n].state = Trit::getStateFromChar(TrinaryMath::getIncrease(value[i].state, tr.value[size-n-1].state));
        }

        if(tmp.isNull() == Trit::T)
            result = result + tmp;
    }

    return result;
}

TrinaryInt TrinaryInt::operator /(TrinaryInt &tr)
{
    TrinaryInt divider = tr;
    TrinaryInt doubleDivider = tr + tr;
    TrinaryInt doubleDividend = (*this) + (*this);

    TrinaryInt tmp;
    TrinaryInt result;

    if(divider.sign() < 0)
        divider.inverse();

    if((divider > TrinaryInt()) == Trit::I)
    {
        for(int i = 0; i < size; i++ )
        {
            tmp.insertBackTrit(doubleDividend.value[i].state);

            if((tmp.abs() > divider.abs()) == Trit::I || ((tmp.abs() > divider.abs()) == Trit::O))
            {
                if(tmp.sign() == divider.sign())
                {
                    tmp = tmp - doubleDivider;
                    result.insertBackTrit(Trit::I);
                }else{
                    tmp = tmp + doubleDivider;
                    result.insertBackTrit(Trit::T);
                }

            }else{
                result.insertBackTrit(Trit::O);
            }
        }

    }

    return result;
}

int TrinaryInt::operator <(const TrinaryInt &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state < tr.value[i].state)
            return Trit::I;
        else if(value[i].state > tr.value[i].state)
            return Trit::T;
    return Trit::O;
}

int TrinaryInt::operator >(const TrinaryInt &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state > tr.value[i].state)
            return Trit::I;
        else if(value[i].state < tr.value[i].state)
            return Trit::T;
    return Trit::O;
}

int TrinaryInt::operator ==(const TrinaryInt &tr)
{
    bool equally = true;
    for(int i = 0; i < size; i++)
        if(value[i].state != tr.value[i].state )
            equally = false;

    if(equally)
        return Trit::I;
    else
        return Trit::T;
}

int TrinaryInt::operator !=(const TrinaryInt &tr)
{
    for(int i = 0; i < size; i++)
        if(value[i].state != tr.value[i].state)
            return Trit::I;
    return Trit::T;
}

TrinaryInt &TrinaryInt::inverse()
{
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            value[i].state = Trit::O;
            break;
        case Trit::T:
            value[i].state = Trit::I;
            break;
        case Trit::I:
            value[i].state = Trit::T;
            break;
        default:
            break;
        }
    }
    return *this;
}

int TrinaryInt::capacity()
{
    for(int i = 0; i < size; i++)
    {
        if(value[i].state != Trit::O)
            return size - i;
    }

    return 0;
}

int TrinaryInt::sign()
{
    for(int i = 0; i < size; i++)
        if(value[i].state != Trit::O)
            return value[i].state;
}

TrinaryInt TrinaryInt::abs()
{
    TrinaryInt tmp = *this;
    if((tmp < TrinaryInt()) == Trit::I)
    {
        return tmp.inverse();
    }else{
        return tmp;
    }
}

int TrinaryInt::isNull()
{
    bool null = true;
    for(int i = 0; i < size; i++)
        if(value[i].state != Trit::O)
            null = false;

    if(null)
        return Trit::I;
    else
        return Trit::T;
}

void TrinaryInt::fromDecimal(int number)
{
    for(int i = 0; i < size; i++)
        value[i].state = Trit::O;

    int newNumber = number;
    if(newNumber < 0)
        newNumber = -newNumber;

    for(int i = size-1; i >= 0; i--)
    {
        int ceil = newNumber/3;
        int res = newNumber % 3;

        switch (res) {
        case 2:
            value[i].state = Trit::T;
            ceil++;
            newNumber = ceil;
            break;
        case 1:
            value[i].state = Trit::I;
            newNumber = ceil;
            break;
        case 0:
            value[i].state = Trit::O;
            newNumber = ceil;
            break;
        default:
            break;
        }

        if(ceil == 0)
            break;
    }

    if(number < 0)
        inverse();
}

int TrinaryInt::toDecimal()
{
    int out = 0;
    for(int i = 0; i < size; i++)
        out += pow(3,(size-i-1))*value[i].state;

    return out;
}

int TrinaryInt::sizeOf()
{
    return size;
}

void TrinaryInt::insertBackTrit(int state)
{
    for(int i = 0; i < size; i++)
    {
        if(i != size-1)
            value[i].state = value[i+1].state;
        else
            value[i].state = state;
    }
}

TrinaryInt &TrinaryInt::fromString(const std::string &str)
{
    if(str.size() == size)
    {
        for(int i = 0; i < size; i++)
            value[i].state = Trit::getStateFromChar(str[i]);
    }else if(str.size() < size){
        for(int i = 0; i < size; i++)
            value[i].state = Trit::O;
        for(int i = 0; i < str.size(); i++)
        {
            value[(size - str.size())+i].state = Trit::getStateFromChar(str[i]);
        }
    }
    return *this;
}

std::string TrinaryInt::toString()
{
    std::string str;
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            str.push_back('O');
            break;
        case Trit::T:
            str.push_back('T');
            break;
        case Trit::I:
            str.push_back('I');
            break;
        default:
            break;
        }
    }
    return str;
}
