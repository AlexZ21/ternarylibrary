#include "tryte.h"

Tryte::Tryte()
{
    value = new Trit[size];
    for(int i = 0; i < size; i++)
        value[i].state = Trit::O;
}

Tryte::~Tryte()
{
    delete value;
}

int Tryte::sizeOf()
{
    return size;
}

void Tryte::fromString(const std::string &str)
{
    if(str.size() == size)
    {
        for(int i = 0; i < size; i++)
            value[i].state = Trit::getStateFromChar(str[i]);
    }else if(str.size() < size){

    }

}

std::string Tryte::toString()
{
    std::string str;
    for(int i = 0; i < size; i++)
    {
        switch (value[i].state) {
        case Trit::O:
            str.push_back('O');
            break;
        case Trit::T:
            str.push_back('T');
            break;
        case Trit::I:
            str.push_back('I');
            break;
        default:
            break;
        }
    }
    return str;
}
