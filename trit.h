#ifndef TRIT_H
#define TRIT_H

class Trit{
public:
    enum {
        T = -1, O = 0, I = 1
    };
    int state;

    static int getStateFromChar(const char &ch)
    {
        switch (ch) {
        case 'T':
            return Trit::T;
            break;
        case 'O':
            return Trit::O;
            break;
        case 'I':
            return Trit::I;
            break;
        }
    }
};

#endif // TRIT_H
