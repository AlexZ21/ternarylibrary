#include "trinarymath.h"

 const char *TrinaryMath::sumTable[3][3] = { "TI", "OT", "OO",
                                             "OT", "OO", "OI",
                                             "OO", "OI", "IT"};
 const char TrinaryMath::incrTable[3][3] = { 'I', 'O', 'T',
                                             'O', 'O', 'O',
                                             'T', 'O', 'I'};

TrinaryMath::TrinaryMath()
{
}

const char *TrinaryMath::getSum(int first, int second)
{
    return sumTable[first+1][second+1];
}

const char TrinaryMath::getIncrease(int first, int second)
{
    return incrTable[first+1][second+1];
}
