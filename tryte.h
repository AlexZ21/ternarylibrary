#ifndef TRYTE_H
#define TRYTE_H

#include "trit.h"
#include <string>

class Tryte
{
    Trit *value;
    const int size = 6;

public:
    Tryte();
    ~Tryte();

    int sizeOf();

    void fromString(const std::string &str);
    std::string toString();
};

#endif // TRYTE_H
