#ifndef TrinaryInt_H
#define TrinaryInt_H

#include "trit.h"
#include "trinarymath.h"
#include <string>
#include <math.h>

class TrinaryInt
{
    Trit *value;
    const int size = 24;

public:
    TrinaryInt();
    TrinaryInt(const TrinaryInt &tr);
    ~TrinaryInt();

    const TrinaryInt& operator = (const TrinaryInt &tr);
    const TrinaryInt& operator = (const int &num);

    TrinaryInt operator +(const TrinaryInt& tr);
    TrinaryInt operator -(const TrinaryInt& tr);

    TrinaryInt operator *(const TrinaryInt& tr);
    TrinaryInt operator /(TrinaryInt& tr);

    int operator <(const TrinaryInt& tr);
    int operator >(const TrinaryInt& tr);
    int operator ==(const TrinaryInt& tr);
    int operator !=(const TrinaryInt& rt);

    TrinaryInt& inverse();
    int capacity();
    int sign();
    TrinaryInt abs();

    int isNull();
    int sizeOf();

    void insertBackTrit(int state);

    void fromDecimal(int number);
    int toDecimal();
    TrinaryInt& fromString(const std::string &str);
    std::string toString();
};

#endif // TrinaryInt_H
