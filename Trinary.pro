#-------------------------------------------------
#
# Project created by QtCreator 2014-03-24T20:24:18
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Trinary
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app


SOURCES += main.cpp \
    tryte.cpp \
    trinarymath.cpp \
    trinaryint.cpp \
    trinaryfloat.cpp

HEADERS += \
    tryte.h \
    trit.h \
    trinarylib.h \
    trinarymath.h \
    trinaryint.h \
    trinaryfloat.h
